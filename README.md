# Customers order Service

This is a simple service to manage customers and their orders. This service can run on either H2 inmemory database or actual database like MYSQL. 

## How to Run

* Clone this repository
* Make sure you are using JDK 1.8 and Maven 3.x
* You can build the project and run the tests by running ```mvn clean package```
* Once successfully built, you can run the service by one of these two methods:
```
        java -jar target/CustomerOrders-0.0.1-SNAPSHOT.jar
or
        mvn spring-boot:run
```
This will run with the default in memory h2 database mode
## About the Service

The service is a simple Customer and Orders API service where we can create new customers, 
query for customer orders,query for customers and post new customer orders.
Below mentioned are the endpoints 

### Get System Information
These endpoints are provided by Spring-boot and Spring-actuator and gives useful information
like endpoints, health , configuration properties, etc.

```
http://localhost:8080/env
http://localhost:8080/health
http://localhost:8080/info
http://localhost:8080/metrics
http://localhost:8080/configprops
http://localhost:8080/mappings
```

### Create a Customer Resource
Will create a Customer resource provided one doesn't exist in the system

```
POST /customers
http://localhost:8080/customers
Content-Type: application/json
REQUEST BODY
{
"id":"101",
"firstName":"Maharshi",
"lastName":"Bhatt",
"email":"maharshi.j.bhatt@gmail.com"
}

RESPONSE: HTTP 201 (Created)
RESPONSE BODY
{
    "id": 101,
    "firstName": "Maharshi",
    "lastName": "Bhatt",
    "email": "maharshi.j.bhatt@gmail.com"
}

IN CASE RESOURCE ALREADY PRESENT
RESPONSE: HTTP 409 (Conflict)
RESPONSE BODY
{
    "timestamp": 1460646716170,
    "status": 409,
    "error": "Conflict",
    "exception": "com.test.exceptions.CustomerExistsException",
    "message": "Customer already Exists",
    "path": "/customers"
}
```

### Querying and Retriving list of Customers
There are 2 querying methods available with Customers, by FirstName 
and/OR by LastName. Each will return a list of matching Customres along 
with their orders. Both parameters are nullable in which case the result
will be list of all Customers. Also unique customer can be addressed via
their ID

```
GET customers
GET customers/?firstName=value&lastName=value
GET customers/{customerId}

http://localhost:8080/customers?firstName=Maharshi&lastName=Bhatt
http://localhost:8080/customers?lastName=Joshi
http://localhost:8080/customers?firstName=Gautami
http://localhost:8080/customers


Response: HTTP 200 (OK)
Content-Type:application/json
[
    {
        "id": 101,
        "firstName": "Maharshi",
        "lastName": "Bhatt",
        "email": "maharshi.j.bhatt@gmail.com",
        "orderList": [
            {
                "orderId": 1,
                "shipLocation": "Sydney",
                "itemDescription": "MacBook"
            }
        ]
    },
    {
        "id": 102,
        "firstName": "Gautami",
        "lastName": "Joshi",
        "email": "gjoshi@gmail.com"
    }
]

```

### Create an order for Customer

```
POST customers/{customerId}/orders
http://localhost:8080/customers/101/orders
Content-Type: application/json

REQUEST BODY
{"shipLocation":"Sydney",
 "itemDescription":"MacBook"
}

RESPONSE: HTTP 201 (Created)
RESPONSE BODY
{
    "orderId": 1,
    "shipLocation": "Sydney",
    "itemDescription": "MacBook"
}

IN CASE POST IS FOR CUSTOMER ID NOT IN DATABASE
RESPONSE: HTTP 400 (Bad Request)
RESPONSE BODY
{
    "timestamp": 1460647885239,
    "status": 400,
    "error": "Bad Request",
    "exception": "com.test.exceptions.CustomerNotFoundForOrder",
    "message": "Customer not found for order",
    "path": "/customers/1200/orders"
}

```

### Return orders for a Customer
Returns list of orders if available or else returns an empty list
```
GET  customers/{customerId}/orders
http://localhost:8080/customers/101/orders/
content-type:application/json

RESPONSE: HTTP 200 (OK)
RESPONSE BODY
[
    {
        "orderId": 1,
        "shipLocation": "Sydney",
        "itemDescription": "MacBook"
    }
]
```

### Returning the single Order for Customer

```
GET customers/{customerid}/orders/{orderid}
http://localhost:8080/customers/1/orders/1

RESPONSE: HTTP 200 (OK)
{
    "orderId": 1,
    "shipLocation": "Sydney",
    "itemDescription": "MacBook"
}
```
# Running the project with MySQL

Have used H2 database as it provide quick way to demonstrate the functionality without 
having to install and setup a database in order to run it. However, it is configured to run with MYSQL database.

Please run the jar with following option to run it with MySQL

### Then run is using the 'mysql' profile:

```
        java -jar -Dspring.profiles.active=mysql target/CustomerOrders-0.0.1-SNAPSHOT.jar
or
        mvn spring-boot:run -Drun.arguments="spring.profiles.active=mysql"
```
### Sample Data and Schema
The sample data and schema are available in resources folder in data.sql and schema.sql. These files 
run automatically and initializes the sample data