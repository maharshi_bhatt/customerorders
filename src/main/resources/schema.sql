drop table orders;
drop table customer;
create table customer (id integer not null, email varchar(255), first_name varchar(255), last_name varchar(255), primary key (id));
create table orders (order_id bigint AUTO_INCREMENT, item_description varchar(255), ship_location varchar(255), customer_id integer, primary key (order_id));
alter table orders add constraint fk_order_customer foreign key (customer_id) references customer (id);