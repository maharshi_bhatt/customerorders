INSERT INTO customer (id, email,first_name, last_name) VALUES ('1001', 'maharshi.j.bhatt@gmai.com', 'Maharshi', 'Bhatt');
INSERT INTO customer (id, email,first_name, last_name) VALUES ('1002', 'gjoshi', 'Gautami', 'Joshi');
INSERT INTO customer (id, email,first_name, last_name) VALUES ('1003', 'achaudhary@gmail.com', 'Arvind', 'Chaudhary');
INSERT INTO customer (id, email,first_name, last_name) VALUES ('1004', 'nrastogi@gmail.com', 'Nitin', 'Rastogi');

INSERT INTO orders (order_id, item_description, ship_location, customer_id) VALUES ('101', 'test_item_1', 'Sydney', '1001');
INSERT INTO orders (order_id, item_description, ship_location, customer_id) VALUES ('102', 'test_item_2', 'Melbourne', '1001');
INSERT INTO orders (order_id, item_description, ship_location, customer_id) VALUES ('103', 'test_item_3', 'Sydney', '1002');
INSERT INTO orders (order_id, item_description, ship_location, customer_id) VALUES ('104', 'test_item_4', 'Perth', '1003');
COMMIT;