package com.test.controller;

import com.test.entity.Customer;
import com.test.entity.Order;
import com.test.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/customers")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Customer> getCustomerOrdersByName(@RequestParam(value = "firstName", required = false) String firstName, @RequestParam(value = "lastName", required = false) String lastName) {

        log.info("Getting data for Customers input parameters firstName {}, lastName {}",firstName,lastName);

        return customerService.getCustomerByFirstAndLastName(firstName, lastName);

    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public Customer createCustomer(@RequestBody Customer customer) {

        log.info("Creating Customer {}", customer);
        return customerService.saveCustomer(customer);

    }

    @RequestMapping(value = "/{id}/orders", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public Order createOrder(@PathVariable int id, @RequestBody Order order) {

        log.info("Posting order for customerid {} with Order {}", id, order);
        return customerService.saveOrder(id, order);
    }

    @RequestMapping(value="/{id}/orders/{orderid}",method = RequestMethod.GET)
    public Order viewSingleOrderforCustomer(@PathVariable int id,@PathVariable int orderid){
        log.info("Getting order id {} for customer id {}",orderid,id);
        return customerService.findSingleOrder(orderid);
    }


    @RequestMapping(value = "/{id}/orders", method = RequestMethod.GET)
    public List<Order> findOrdersByCustomer(@PathVariable int id) {
        log.info("Getting orders for Customer id {}", id);
        return customerService.findOrdersByCustomer(id);

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Customer findCustomer(@PathVariable int id) {
        log.info("Getting Customer info for Customer id {}", id);
        return customerService.findCustomer(id);

    }


}
