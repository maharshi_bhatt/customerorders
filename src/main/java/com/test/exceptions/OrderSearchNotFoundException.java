package com.test.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.METHOD_NOT_ALLOWED, reason = "Search method for Order Not found")
public class OrderSearchNotFoundException extends RuntimeException {
}
