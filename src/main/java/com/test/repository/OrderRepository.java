package com.test.repository;

import com.test.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {

    List<Order> findByCustomerId(int customerId);

    List<Order> findByCustomerFirstName(String firstName);


}
