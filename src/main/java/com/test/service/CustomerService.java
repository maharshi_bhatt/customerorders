package com.test.service;

import com.google.common.collect.Lists;
import com.test.entity.Customer;
import com.test.entity.Order;
import com.test.exceptions.CustomerExistsException;
import com.test.exceptions.CustomerNotFound;
import com.test.exceptions.CustomerNotFoundForOrder;
import com.test.repository.CustomerRepository;
import com.test.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Component
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private OrderRepository orderRepository;

    public List<Customer> getAllCustomers() {
        return Lists.newArrayList(customerRepository.findAll());
    }

    public Customer saveCustomer(Customer customer) {
        if (customerRepository.exists(customer.getId())) {
            log.error("Customer already Exists:" + customer);
            throw new CustomerExistsException();
        }
        return customerRepository.save(customer);
    }

    public Order saveOrder(int id, Order order) {
        Optional<Customer> customer = Optional.ofNullable(customerRepository.findOne(id));

        order.setCustomer(customer.orElseThrow(() -> {
            log.error("Customer id {} not found.",id);
            return new CustomerNotFoundForOrder();
        }));
        return orderRepository.save(order);
    }

    public List<Order> findOrdersByCustomer(int id) {
        log.info("Finding by CustomerId {}", id);
        return orderRepository.findByCustomerId(id);
    }

    public Order findSingleOrder(int orderid){
        log.info("Finding by OrderId {}",orderid);
        return orderRepository.findOne(Integer.toUnsignedLong(orderid));
    }

    public Customer findCustomer(int id) {
        Optional<Customer> customer = Optional.ofNullable(customerRepository.findOne(id));
        log.info("In find customer method for id {} Customer exists {}",id,customer.isPresent());
        return customer.orElseThrow(() -> new CustomerNotFound());
    }

    public List<Customer> getCustomerByFirstAndLastName(String firstName, String lastName) {
        if(Objects.isNull(firstName) && Objects.isNull(lastName)){
            log.info("Returning findAll() as both firstName and lastName are null.");
            return customerRepository.findAll();
        }
        log.info("Finding object for firstName {} and/or lastName {} ",firstName,lastName);
        return customerRepository.findByFirstNameOrLastName(firstName,lastName);
    }
}
