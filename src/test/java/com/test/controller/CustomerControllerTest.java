package com.test.controller;

import com.test.CustomerOrdersApplicationTests;
import com.test.entity.Customer;
import com.test.entity.Order;
import com.test.repository.CustomerRepository;
import com.test.repository.OrderRepository;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class CustomerControllerTest extends CustomerOrdersApplicationTests{

    private MockMvc mvc;
    @Autowired
    private CustomerController customerController;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private OrderRepository orderRepository;

    @Before
    public void setUp() throws Exception {
        mvc = MockMvcBuilders.standaloneSetup(customerController).build();
        //delete all records
        orderRepository.deleteAllInBatch();
        customerRepository.deleteAllInBatch();
    }

    //Methods to test Customer Querying Scenario
    @Test
    public void shouldReturnEmptyCollection() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/customers"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    public void shouldReturnCustomerValue() throws Exception {
        customerRepository.save(Customer.builder().id(1).firstName("Maharshi").lastName("Bhatt").build());
        mvc.perform(MockMvcRequestBuilders.get("/customers/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName",equalTo("Maharshi")));
    }

    @Test
    public void shouldReturnCustomers() throws Exception {
        customerRepository.save(Customer.builder().id(1).firstName("Test").lastName("Test").build());
        mvc.perform(MockMvcRequestBuilders.get("/customers"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(1)))
                .andExpect(jsonPath("$[0].firstName",equalTo("Test")));
    }

    @Test
    public void shouldReturnCustomerOrdersFirstName() throws Exception{
        Customer customer = customerRepository.save(Customer.builder().id(1).firstName("Maharshi").lastName("Test").build());
        Customer customer2 = customerRepository.save(Customer.builder().id(2).firstName("Arvind").lastName("Test").build());

        mvc.perform(MockMvcRequestBuilders.get("/customers").param("firstName","Maharshi"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$[0].firstName",equalTo("Maharshi")));

    }

    @Test
    public void shouldReturnCustomerOrdersLastName() throws Exception{
        Customer customer = customerRepository.save(Customer.builder().id(1).firstName("Maharshi").lastName("Bhatt").build());
        Customer customer1 = customerRepository.save(Customer.builder().id(2).firstName("Arvind").lastName("Chaudhary").build());
        mvc.perform(MockMvcRequestBuilders.get("/customers").param("lastName","Chaudhary"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].lastName",equalTo("Chaudhary")));

    }

    @Test
    public void shouldReturnCustomerOrdersBYFirstAndLastName() throws Exception{
        Customer customer = customerRepository.save(Customer.builder().id(1).firstName("Maharshi").lastName("Bhatt").build());
        Customer customer1 = customerRepository.save(Customer.builder().id(2).firstName("Arvind").lastName("Chaudhary").build());
        mvc.perform(MockMvcRequestBuilders.get("/customers").param("lastName","Chaudhary").param("firstName","Arvind"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$[0].lastName",equalTo("Chaudhary")));

    }

    //Methods to test the Customer Creation Scenario
    @Test
    public void shouldCreateCustomer() throws Exception{
        mvc.perform((MockMvcRequestBuilders.
                        post("/customers").contentType(MediaType.APPLICATION_JSON)
                        .content(json(Customer.builder().
                                id(1001).
                                firstName("Maharshi")
                                .lastName("Bhatt")
                                .email("mymail@mail.com").build()))))
                        .andExpect(status().isCreated())
                        .andExpect(jsonPath("$.firstName",equalTo("Maharshi")));
    }

    @Test
    public void shouldReturnSingleOrderforCustomer() throws Exception{
        Customer customer = customerRepository.save(Customer.builder().id(1).firstName("Maharshi").lastName("Bhatt").build());
        Order order = orderRepository.save(Order.builder().customer(customer).itemDescription("test_item_1").build());
        orderRepository.save(Order.builder().customer(customer).itemDescription("test_item_2").build());
        mvc.perform((MockMvcRequestBuilders.
                get("/customers/1/orders/"+order.getOrderId())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.itemDescription",equalTo("test_item_1")));
    }

    //Methods to test Order Creation Scenario
    @Test
    public void shouldCreateOrder() throws Exception{
        customerRepository.save(Customer.builder().id(1).firstName("Test").lastName("Test").build());
        mvc.perform((MockMvcRequestBuilders.
                post("/customers/1/orders").
                content(json(Order.builder().itemDescription("test Item").shipLocation("Sydney").build()))).
                contentType(MediaType.APPLICATION_JSON))
                                            .andExpect(status().isCreated())
                                            .andExpect(jsonPath("$.itemDescription",equalTo("test Item")));

    }

    //Methods to test Order Query Scenarios
    @Test
    public void shouldReturnOrdersByCustomer() throws Exception{
        Customer customer = customerRepository.save(Customer.builder().id(1).firstName("Test").lastName("Test").build());
        orderRepository.save(Order.builder().customer(customer).itemDescription("test_item_1").build());
        orderRepository.save(Order.builder().customer(customer).itemDescription("test_item_2").build());

        mvc.perform((MockMvcRequestBuilders.
                get("/customers/1/orders")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(2)));
    }

}